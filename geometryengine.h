#ifndef GEOMETRYENGINE_H
#define GEOMETRYENGINE_H

#include <QGLFunctions>
#include <QGLShaderProgram>
#include <QFile>
#include <QVector2D>
#include <QVector3D>
#include <QDebug>

class GeometryEngine : protected QGLFunctions
{
public:
    GeometryEngine();
    virtual ~GeometryEngine();

    void init();
    void drawCubeGeometry(QGLShaderProgram *program);

private:
    struct VertexData
    {
        QVector3D position;
        QVector3D normal;
        QVector3D color;
    };

    void initCubeGeometry();
    void loadGeometry(QString hdr, QString dem);
    GLuint vboIds[2];
    VertexData* vertices;
    GLuint* indices;

    int nRows;
    int nCols;
    int frameCounter;

    int indexCount;
};

#endif // GEOMETRYENGINE_H
