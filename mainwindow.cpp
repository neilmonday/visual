#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
#ifndef QT_NO_OPENGL
    glWidget = new GLWidget();
    ui->tabWidget->addTab(glWidget, QString("OpenGL"));
#else
    QLabel note("OpenGL Support required");
    note.show();
#endif


}

MainWindow::~MainWindow()
{
    delete ui;
}
