QT += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Visual
TEMPLATE = app

SOURCES += \
    main.cpp\
    mainwindow.cpp \
    glwidget.cpp \
    geometryengine.cpp

HEADERS += \
    mainwindow.h \
    glwidget.h \
    geometryengine.h

FORMS += \
    mainwindow.ui

RESOURCES += \
    shaders.qrc
