#include "geometryengine.h"

GeometryEngine::GeometryEngine()
{
    indexCount = 0;
}

GeometryEngine::~GeometryEngine()
{
    glDeleteBuffers(2, vboIds);
}

void GeometryEngine::init()
{
    initializeGLFunctions();

    // Generate 2 VBOs
    glGenBuffers(2, vboIds);

    // Load the geometry from a file.
    loadGeometry(QString("..\\visual\\n37w110\\floatn37w110_1.hdr"), QString("..\\visual\\n37w110\\floatn37w110_1.flt"));

    // Initializes cube geometry and transfers it to VBOs
    initCubeGeometry();
}

void GeometryEngine::loadGeometry(QString hdr, QString dem) {

    QFile hdrFile(hdr);
    if(!hdrFile.open(QIODevice::ReadOnly)) return;
    //QByteArray byteOrderLine = hdrFile.readLine(64);
    //QByteArray layoutLine = hdrFile.readLine(64);
    QByteArray nRowsLine = hdrFile.readLine(64);
    QByteArray nColsLine = hdrFile.readLine(64);
    QByteArray nBandsLine = hdrFile.readLine(64);
    QByteArray nBitsLine = hdrFile.readLine(64);

    //QString byteOrderString = QString(byteOrderLine).split(" ")[1];
    //QString layoutString = QString(layoutLine).split(" ")[1];
    nRows = QString(nRowsLine).split(" ", QString::SkipEmptyParts)[1].toInt();
    nCols = QString(nColsLine).split(" ", QString::SkipEmptyParts)[1].toInt();
    int nBands = QString(nBandsLine).split(" ", QString::SkipEmptyParts)[1].toInt();
    int nBits = QString(nBitsLine).split(" ", QString::SkipEmptyParts)[1].toInt();

    //nCols = 10;
    //nRows = 55;

    indexCount = (nCols * nRows) + (nCols * (nRows-2)) + ((nRows*2) - 4);
    vertices = new VertexData[nCols * nRows];//[nRows * nCols];
    indices = new GLuint[indexCount];//[nRows * nCols];

    QFile demFile(dem);
    if(!demFile.open(QIODevice::ReadOnly)) return;
    QByteArray blob = demFile.readAll();

    //GLuint index = 0; //this is used for indeces
    //int k = 0;

    unsigned int offset = 0;

    // First, build the data for the vertex buffer
    for (int y = 0; y < nRows; y++) {
        qDebug() << y;
        for (int x = 0; x < nCols; x++) {
            //int elevation = ((blob.at(2*offset+1) << 8) & 0x0000FF00) + (blob.at(2*offset) & 0x000000FF);
            int elevation = ((blob.at(4*offset+1) << 24) & 0x0000F000) + ((blob.at(4*offset+1) << 16) & 0x00000F00) + ((blob.at(4*offset+1) << 8) & 0x000000F0) + (blob.at(4*offset) & 0x0000000F);
                    //(blob[(4*offset)] >> CHAR_BIT*2) + (blob[((4*offset)+1)] >> CHAR_BIT*3) + (blob[((4*offset)+2)] >> CHAR_BIT*0) + (blob[((4*offset)+3)] >> CHAR_BIT*1);
            elevation *= -1;
            if(elevation < -3000)
            {
                qDebug() << "elevation error: " << elevation;
                elevation = vertices[offset-1].position.z();
            }
            vertices[offset].position = QVector3D(x-600, y-600, (elevation-720)/50);

            // Calculate the normal using the cross product of the slopes.
            if(offset > 1)
            {
                vertices[offset].normal = vertices[offset].position.normal(vertices[offset-1].position, vertices[offset-2].position);
            }
            else
            {
                vertices[offset].normal = QVector3D(0, 0, 1);
            }

            // Add some fancy colors.

            //qDebug() << "Elevation: " << elevation*-1;
            vertices[offset].color = QVector3D((((float)elevation*-1))/500, (((float)elevation*-1))/500, (((float)elevation*-1))/500);

            offset++;
        }
    }

    offset = 0;
    for (unsigned int y = 0; y < (nRows-1); y++)
    {
        if (y > 0) {
            // Degenerate begin: repeat first vertex
            indices[offset++] = (y * nCols);
        }
        for (unsigned int x = 0; x < nCols; x++) {
            // One part of the strip
            indices[offset++] = (y * nCols) + x;//((y * yLength) + x);
            indices[offset++] = ((y+1) * nCols) + x;//(((y + 1) * yLength) + x);
        }
        if (y < (nRows-2)) {
            // Degenerate end: repeat last vertex
            indices[offset++] = (((y + 1) * nCols) + (nCols - 1));
        }
    }

    //qDebug() << "Done";
}

void GeometryEngine::initCubeGeometry()
{
    glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);
    glBufferData(GL_ARRAY_BUFFER, (nRows * nCols) * sizeof(VertexData), vertices, GL_STATIC_DRAW);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIds[1]);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER,  indexCount * sizeof(GLuint), indices, GL_STATIC_DRAW);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}

void GeometryEngine::drawCubeGeometry(QGLShaderProgram *program)
{
    static char* POSITION_ATTRIBUTE = "a_Position";
    static char* NORMAL_ATTRIBUTE = "a_Normal";
    static char* COLOR_ATTRIBUTE = "a_Color";

    int positionAttribute = program->attributeLocation(POSITION_ATTRIBUTE);
    int normalAttribute = program->attributeLocation(NORMAL_ATTRIBUTE);
    int colorAttribute = program->attributeLocation(COLOR_ATTRIBUTE);

    glClearColor(0.75, 0.75, 0.75, 1.0);
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glBindBuffer(GL_ARRAY_BUFFER, vboIds[0]);

    program->enableAttributeArray(positionAttribute);
    glVertexAttribPointer(positionAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)0);
    program->enableAttributeArray(normalAttribute);
    glVertexAttribPointer(normalAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)(sizeof(QVector3D)));
    program->enableAttributeArray(colorAttribute);
    glVertexAttribPointer(colorAttribute, 3, GL_FLOAT, GL_FALSE, sizeof(VertexData), (const void *)(2*(sizeof(QVector3D))));

    // Draw
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, vboIds[1]);
    glDrawElements(GL_TRIANGLE_STRIP, indexCount, GL_UNSIGNED_INT, 0);

    //clear the buffers
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
}
